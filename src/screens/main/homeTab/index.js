import {useNavigation} from '@react-navigation/core';
import React from 'react';
import {View, Text, Button} from 'react-native';
import router from '../../../navigation/router';
import Timeline from '../../../component/Timeline';
import Header from '../../../component/Header';

export default function HomeScreen() {
  const navigation = useNavigation();
  return (
    <View>
      <Header />
      <Timeline />
    </View>
  );
}
