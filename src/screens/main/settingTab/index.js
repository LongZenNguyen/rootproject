import React from 'react';
import {View, Text, Button} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default function SettingScreen(props) {
  return (
    <View>
      <Button title="Sign Out" onPress={AsyncStorage.removeItem('tokenn')} />
    </View>
  );
}
