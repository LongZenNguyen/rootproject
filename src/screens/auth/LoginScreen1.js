import React, {useState} from 'react';

import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  ActivityIndicator,
  Dimensions,
  Image,
} from 'react-native';
import {useDispatch} from 'react-redux';
import {baseAPI} from '../../config/baseApi';
import {saveToken} from '../../redux/slice/authSlice';

const _WIDTH = Dimensions.get('window').width;
const _HEIGHT = Dimensions.get('window').height;

export default function Login() {
  const [loading, setLoading] = useState(false);
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const dispatch = useDispatch();

  const _onChangeTextUsername = value => {
    setUsername(value);
  };

  const _onChangeTextPassword = value => {
    setPassword(value);
  };

  const onLogin = () => {
    setLoading(true);
    baseAPI
      .post('/auth/login', {
        email: username,
        password: password,
      })
      .then(res => {
        setLoading(false);
        const token = res.data.token;
        dispatch(saveToken(token));
        console.log('response = ', res);
      })
      .catch(err => {
        setLoading(false);
        console.log('errrr ', err);
      });
  };
  //slide show

  //============end slide show===============
  return loading ? (
    <ActivityIndicator />
  ) : (
    <View style={{flex: 1, marginTop: 50, marginHorizontal: 20}}>
      <View>
        <Image
          style={styles.logo}
          source={require('../../assets/logo/fpt.png')}
        />
        <Text>Tài Khoản:</Text>
        <TextInput
          placeholder="Please input username"
          placeholderTextColor="gray"
          style={styles.textInput}
          onChangeText={_onChangeTextUsername}
        />

        <Text style={{marginTop: 16}}>Mật Khẩu:</Text>
        <TextInput
          placeholder="Please input your password"
          placeholderTextColor="gray"
          style={styles.textInput}
          secureTextEntry={true}
          onChangeText={_onChangeTextPassword}
        />
      </View>

      <TouchableOpacity style={styles.loginBottom} onPress={onLogin}>
        <Text style={styles.loginText}>Đăng Nhập</Text>
      </TouchableOpacity>

      <TouchableOpacity style={styles.otpButton} onPress={onLogin}>
        <Text style={styles.loginText}>Đăng Nhập bằng OTP</Text>
      </TouchableOpacity>

      <View style={{flexDirection: 'row', alignItems: 'center'}}>
        <View style={{flex: 1, height: 1, backgroundColor: 'black'}} />
        <View>
          <Text style={{width: 50, textAlign: 'center'}}> Hoặc </Text>
        </View>
        <View style={{flex: 1, height: 1, backgroundColor: 'black'}} />
      </View>

      <TouchableOpacity style={{alignSelf: 'center'}} onPress={onLogin}>
        <Image
          style={styles.otp}
          source={require('../../assets/logo/google.png')}
        />
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  textInput: {
    padding: 10,
    height: 50,
    borderWidth: 1,
    borderRadius: 20,
    marginTop: 4,
  },
  loginBottom: {
    marginTop: 30,
    backgroundColor: '#F27125',
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 18,
    width: _WIDTH / 1.1,
    height: _HEIGHT / 13.5,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,

    elevation: 10,
  },
  loginText: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 18,
  },
  otpButton: {
    marginTop: 20,
    marginBottom: 30,
    backgroundColor: '#F27125',
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 18,
    width: _WIDTH / 1.1,
    height: _HEIGHT / 13.5,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,

    elevation: 10,
  },
  logo: {
    width: '100%',
    height: 125,
    alignSelf: 'center',
    resizeMode: 'contain',
    marginTop: 5,
    marginBottom: 30,
  },
  otp: {
    height: 160,
    width: 160,
    resizeMode: 'contain',
    alignSelf: 'center',
  },
});
