import React, {useEffect} from 'react';
import {View, Image} from 'react-native';
import {Dimensions} from 'react-native';
export default function SplashScreen(props) {
  useEffect(() => {
    const timeout = setTimeout(() => {
      // do something
      props.navigation.navigate('LoginScreen');
    }, 3000);
    return () => {
      clearTimeout(timeout);
    };
  }, [props.navigation]);
  return (
    <View style={styles.viewStyles}>
      <Image style={styles.imageStyles} source={require('./ic_launcher.png')} />
    </View>
  );
}

const styles = {
  viewStyles: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#f9fdff',
  },
  imageStyles: {
    width: '50%',
    height: '30%',
    resizeMode: 'contain',
  },
};
