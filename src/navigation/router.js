export default {
  authStack: 'AuthStack',
  mainStack: 'MainStack',
  homeDetailStack: 'Home',
  settingDetailStack: 'Setting',
  chatDetailStack: 'Chat',
  reportDetailStack: 'Report',

  login: 'LoginScreen',
  register: 'RegisterScreen',
  splash: 'SplashScreen',

  home: 'HomeScreen',
  setting: 'SettingScreen',
  listSong: 'ListSongScreen',
  chat: 'ChatScreen',
  report: 'ReportScreen',
};
