import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import router from '../router';
const Stack = createStackNavigator();
import LoginScreen from '../../screens/auth/LoginScreen';
import RegisterScreen from '../../screens/auth/RegisterScreen';
import SplashScreen from '../../screens/auth/SplashScreen';

export default AuthStack = () => {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name={router.splash} component={SplashScreen} />
      <Stack.Screen name={router.login} component={LoginScreen} />
      <Stack.Screen name={router.register} component={RegisterScreen} />
    </Stack.Navigator>
  );
};
