import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import router from '../router';
const Stack = createStackNavigator();
import ChatScreen from '../../screens/main/chatTab';

export default function ChatDetailStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen name={router.chatDetailStack} component={ChatScreen} />
    </Stack.Navigator>
  );
}
