import React from 'react';
import {View, Text} from 'react-native';

import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {NavigationContainer} from '@react-navigation/native';
import router from '../router';
const Tab = createBottomTabNavigator();
import HomeDetailStack from './HomeDetailStack';
import SettingDetailStack from './SettingDetailStack';
import ReportDetailStack from './ReportDetailStack';
import ChatDetailStack from './ChatDetailStack';
import Header from '../../component/Header';

export default function Navigation() {
  return (
    <View>
      <Header />
      <Tab.Navigator>
        <Tab.Screen name={router.homeDetailStack} component={HomeDetailStack} />
        <Tab.Screen
          name={router.reportDetailStack}
          component={ReportDetailStack}
        />
        <Tab.Screen name={router.chatDetailStack} component={ChatDetailStack} />
        <Tab.Screen
          name={router.settingDetailStack}
          component={SettingDetailStack}
        />
      </Tab.Navigator>
    </View>
  );
}
