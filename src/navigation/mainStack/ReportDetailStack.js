import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import router from '../router';
const Stack = createStackNavigator();
import ReportScreen from '../../screens/main/reportTab';

export default function ReportDetailStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen name={router.reportDetailStack} component={ReportScreen} />
    </Stack.Navigator>
  );
}
