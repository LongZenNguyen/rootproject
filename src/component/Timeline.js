/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Timeline from 'react-native-timeline-flatlist';

export default function App() {
  const data = [
    {
      time: '09:00',
      title: 'Home Training',
      description:
        'The Beginner Archery and Beginner Crossbow course does not require you to bring any equipment, since everything you need will be provided for the course. ',
      circleColor: '#F27125',
    },
    {
      time: '10:45',
      title: 'Play Badminton',
      description:
        'Badminton is a racquet sport played using racquets to hit a shuttlecock across a net.',
    },
    {time: '12:00', title: 'Lunch'},
    {
      time: '14:00',
      title: 'Watch Soccer',
      description:
        'Team sport played between two teams of eleven players with a spherical ball. ',
    },
    {
      time: '16:30',
      title: 'Go to Fitness center',
      description: 'Look out for the Best Gym & Fitness Centers around me :)',
    },
    {
      time: '17:30',
      title: 'Go to Fitness center',
      description: 'Look out for the Best Gym & Fitness Centers around me :)',
    },
    {
      time: '18:30',
      title: 'Go to Fitness center',
      description: 'Look out for the Best Gym & Fitness Centers around me :)',
    },
  ];

  //'rgb(45,156,219)'
  return (
    <View style={styles.container}>
      <Timeline
        style={styles.list}
        data={data}
        separator={true}
        circleSize={20}
        circleColor="#F27125"
        lineColor="gray"
        timeContainerStyle={{minWidth: 52, marginTop: 0}}
        timeStyle={{
          textAlign: 'center',
          backgroundColor: '#F27125',
          color: 'black',
          padding: 5,
          borderRadius: 13,
          overflow: 'hidden',
        }}
        descriptionStyle={{color: '#003751'}}
        options={{
          style: {paddingTop: 5},
        }}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    padding: 20,
    paddingTop: 30,
  },
  list: {
    flex: 1,
    marginTop: 20,
  },
});
