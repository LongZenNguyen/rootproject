import React from 'react';
import {View, Text} from 'react-native';

import {Button, Header, Icon} from 'react-native-elements';

export default function index() {
  const notificationIcon = <Icon name="sc-telegram" />;
  return (
    <View>
      <Header
        statusBarProps={{barStyle: 'default-content'}}
        containerStyle={{
          backgroundColor: '#F27125',
          justifyContent: 'space-around',
        }}
        leftComponent={{
          icon: 'menu',
          color: '#fff',
          iconStyle: {color: '#fff'},
        }}
        centerComponent={{text: 'HOME', style: {color: '#fff'}}}
        rightComponent={{icon: {notificationIcon}, color: '#fff'}}
      />
    </View>
  );
}
