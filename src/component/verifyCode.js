import React, {Component, useState, useEffect} from 'react';
import {
  FlatList,
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  Image,
  View,
  ImageBackground,
  Alert,
} from 'react-native';
import SMSVerifyCode from 'react-native-sms-verifycode';
import {Button} from 'react-native-elements';
//navigation library
import 'react-native-gesture-handler';
import {
  NavigationContainer,
  useNavigation,
  useFocusEffect,
  navigate,
  navigation,
} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {Tab_copy} from './Tab_copy';

export default function SlideshowTest({props}) {
  var codeVerify = 123456;
  const onInputCompleted1 = text => {
    Alert.alert(text);
  };
  return (
    <View backgroundColor="#F27125" flex={1}>
      <Text style={styles.titleText}>Vui lòng nhập mã Code gồm 6 chữ số</Text>
      <SMSVerifyCode
        verifyCodeLength={6}
        name="verifycode"
        containerPaddingVertical={20}
        containerPaddingHorizontal={20}
        containerBackgroundColor="#00000000"
        codeViewBorderColor="white"
        focusedCodeViewBorderColor="grey"
        codeViewBorderWidth={3}
        codeViewBorderRadius={10}
        fontWeight="bold"
        codeFontSize={26}
        codeColor="white"
        warningTitle="Cảnh báo"
        warningContent="Chỉ được nhập số!"
        warningButtonText="Đồng ý"
        opacity={1}
        marginHorizontal={10}
        onInputCompleted={onInputCompleted1}
      />
      <View
        marginTop={20}
        // numColumns={2}
        flexDirection="row"
        marginLeft={0}
        justifyContent="space-evenly">
        <TouchableOpacity style={styles.buttonStyle}>
          <Text style={styles.appButtonText}>Reset</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.buttonStyle}
          // onPress={() => navigation.navigate('./Tab_copy')}
          // title="go to Tab_copy"
        >
          <Text style={styles.appButtonText}>Hủy</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}
const styles = StyleSheet.create({
  titleText: {
    marginHorizontal: -50,
    marginVertical: 60,
    paddingHorizontal: 150,
    fontSize: 22,
    color: 'white',
    textAlign: 'center',
    fontWeight: 'bold',
  },
  buttonStyle: {
    elevation: 8,
    backgroundColor: 'white',
    borderRadius: 10,
    paddingVertical: 15,
    width: 170,
    // padd
    paddingHorizontal: -12,
    alignSelf: 'center',
    shadowColor: 'grey',
    shadowOpacity: 0.8,
    shadowRadius: 20,
  },
  appButtonText: {
    fontSize: 18,
    color: '#F27125',
    fontWeight: 'bold',
    alignSelf: 'center',
    textTransform: 'uppercase',
  },
  image: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
    opacity: 0.8,
    backgroundColor: 'rgba(52, 52, 52, 0.5)',
  },
});
