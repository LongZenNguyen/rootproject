import React, {Component, useState, useEffect} from 'react';
import {
  FlatList,
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  Image,
} from 'react-native';
import Slideshow from 'react-native-image-slider-show';

// ...
export default function SlideshowTest(props) {
  let dataU = [
    {
      title: 'hạnh óc chó',
      caption: 'Ngu vãi cả LOL',
      url: 'https://scontent.fpnh22-3.fna.fbcdn.net/v/t1.6435-9/90171574_2664410947166154_399992786767577088_n.jpg?_nc_cat=109&ccb=1-3&_nc_sid=174925&_nc_ohc=k6dulFUQXiUAX9k_3uv&_nc_oc=AQlWH-Gejd-Ch-rwD_WMPxa3f8ijtAiKzqeCJo6NchT7q6SZLugaakyMzEzUWSQQKEE&_nc_ht=scontent.fpnh22-3.fna&oh=34cc9023429f5ff3bea073ad267ef95a&oe=60C9F9CD',
    },
    {
      title: 'hạnh đầu boài',
      caption: 'Bú boài',
      url: 'https://scontent.fpnh22-2.fna.fbcdn.net/v/t31.18172-8/25587960_2020813808192541_4477995742147596097_o.jpg?_nc_cat=110&ccb=1-3&_nc_sid=84a396&_nc_ohc=lux6VWFrMnEAX8LrvuT&_nc_ht=scontent.fpnh22-2.fna&oh=2292a246542b35108cf59698f2dc1856&oe=60EBA178',
    },
    {
      title: 'Hạnh mặt LOL',
      caption: 'Mặt LOL',
      url: 'https://scontent.fpnh22-1.fna.fbcdn.net/v/t1.18169-9/17523458_1879155405691716_2623848828831287119_n.jpg?_nc_cat=101&ccb=1-3&_nc_sid=a9b1d2&_nc_ohc=fX5YGRtm49gAX-RUdjF&_nc_ht=scontent.fpnh22-1.fna&oh=b0566e52d479a25a633e1eb1218eacca&oe=60CA2F7C',
    },
  ];
  const [position, setPosition] = useState(1);
  const [interval, setInterval] = useState(2000);
  const [dataSource, setDataSource] = useState(dataU);
  // a[0] a[1] a[2]
  const componentWillMount = e => {
    console.log('Run here');
    setInterval({
      interval: setInterval(() => {
        console.log('Position : ' + position + ' - Interval: ' + interval);
        setPosition({
          position: position === dataSource.length ? 0 : position + 1,
        });
      }, 3000),
    });
  };
  useEffect(() => {
    clearInterval(setInterval);
  }, []);
  return (
    <Slideshow
      dataSource={dataSource}
      position={position}
      onPositionChanged={position => setPosition({position})}
    />
  );
}
// const slideshowTest = new SlideshowTest();
// export default slideshowTest;
